(ns aero.aws
  (:require [aero.core :as aero]
            [camel-snake-kebab.core :refer [->kebab-case-keyword]]
            [cheshire.core :as json])
  (:import com.fasterxml.jackson.core.JsonParseException
           software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest
           software.amazon.awssdk.services.secretsmanager.SecretsManagerClient))

(defn parse-secret-string
  [response]
  (let [string (.secretString response)]
    (or
     (try
       (json/parse-string string ->kebab-case-keyword)
       (catch JsonParseException _))
     string)))

(defn get-secret-value
  [secret-id]
  (let [client (SecretsManagerClient/create)
        request (.build (doto (GetSecretValueRequest/builder)
                          (.secretId secret-id)))
        response (.getSecretValue client request)]
    {:name (.name response)
     :secret (or (parse-secret-string response) (.secretBinary response))
     :version-id (.versionId response)
     :arn (.arn response)
     :created-data (.createdDate response)
     :version-stages (.versionStages response)}))

(defmethod aero/reader 'aws/secret
  [_ tag value]
  (:secret (get-secret-value (str value))))
